from os import system as cmd


def install_docker():
    """Install Docker"""
    
    # Start an interactive sudo session
    cmd('sudo -i')

    # Install dependencies
    cmd('apt-get update')
    cmd('apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common')

    # Install Docker
    cmd('curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -')
    cmd('add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"')
    cmd('apt-get update')
    cmd('apt-get install docker-ce docker-ce-cli containerd.io')

    # Test Docker
    cmd('docker run hello-world')
    cmd('docker version')


def install_docker_compose(version: str = 'v2.2.3'):
    """Install Docker Compose
    
    OBS! Use the latest version available here: https://github.com/docker/compose/releases
    """

    # UPDATE THE VERSION NUMBER BELOW TO THE LATEST VERSION AVAILABLE HERE: https://github.com/docker/compose/releases
    cmd(f'curl -L "https://github.com/docker/compose/releases/download/{version}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose')

    # Make the binary executable
    cmd('sudo chmod +x /usr/local/bin/docker-compose')

    # Test Docker Compose
    cmd('docker-compose version')
