from rich import print

from docker import install_docker
from docker import install_docker_compose


def main():
    """Guided setup for Docker, Traefik, and Synapse"""

    install_docker()
    install_docker_compose('v2.2.3')


if __name__ == '__main__':
    main()