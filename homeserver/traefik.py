from subprocess import run as cmd
from pathlib import Path
import yaml
import time



class TraefikContainer:
    def __init__(self, directory, username, password, email_address, domain):
        self.directory = Path(directory)
        self.username = username
        self.password = password
        
        self.email_address = email_address
        self.domain = domain

        self.install()
        
    def install(self):
        """Install and configure Traefik"""
        
        # Start an interactive sudo session
        cmd('sudo -i')

        # Install the htpasswd tool which will be used
        # to create a string to securely store a password for Traefik later
        cmd('apt-get update')
        cmd('apt-get install apache2-utils')

        # Create the following files and directories
        
        cmd(f'mkdir -p {self.directory / "data"}')  
        cmd(f'cd {self.directory}')

        cmd(f'touch {self.directory / "docker-compose.yml"}')
        cmd(f'touch {self.directory / "data" / "traefik.yml"}')
        cmd(f'touch {self.directory / "data" / "dynamic_conf.yml"}')

        cmd(f'touch {self.directory / "data" / "acme.json"}')
        cmd(f'chmod 600 {self.directory / "data" / "acme.json"}')

        # Set up configuration
        self.create_dynamic_conf_yml()
        self.create_traefik_yml()
        self.generate_password_string()

        # Configure and build Docker container
        self.configure_docker_compose()
        self.build_docker_container()
        

    def create_dynamic_conf_yml(self):
        """Initialise `./data/dynamic_conf.yml`"""

        with open(self.directory / 'data' / 'dynamic_conf.yml', 'w') as f:
            yaml.dump({
                'tls': {
                    'options': {
                        'default': {
                            'minVersion': 'VersionTLS12',
                            'cipherSuites': [
                                'TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256',
                                'TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384',
                                'TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305',
                                'TLS_AES_128_GCM_SHA256',
                                'TLS_AES_256_GCM_SHA384',
                                'TLS_CHACHA20_POLY1305_SHA256'
                            ],
                            'curvePreferences': ['CurveP521', 'CurveP384'],
                            'sniStrict': True
                        }
                    }
                },
                'http': {
                    'middlewares': {
                        'secHeaders': {
                            'headers': {
                                'browserXssFilter': True,
                                'contentTypeNosniff': True,
                                'frameDeny': True,
                                'sslRedirect': True,
                                'stsIncludeSubdomains': True,
                                'stsPreload': True,
                                'stsSeconds': 31536000,
                                'customFrameOptionsValue': 'SAMEORIGIN'
                            }
                        }
                    }
                }
            }, f)

    def create_traefik_yml(self):
        """Initialise `./data/traefik.yml`"""

        with open(self.directory / 'data' / 'dynamic_conf.yml', 'w') as f:
            yaml.dump({
                'api': {
                    'dashboard': True
                },
                'entryPoints': {
                    'http': {
                        'address': ':80'
                    },
                    'https': {
                        'address': ':443'
                    }
                },
                'providers': {
                    'docker': {
                        'endpoint': 'unix:///var/run/docker.sock',
                        'exposedByDefault': False
                    },
                    'file': {
                        'filename': './dynamic_conf.yml'
                    }
                },
                'certificatesResolvers': {
                    'http': {
                        'acme': {
                            'email': self.email_address,
                            'storage': 'acme.json',
                            'httpChallenge': {
                                'entryPoint': 'http'
                            }
                        }
                    }
                }
            }, f)

    def generate_password_string(self):
        """Configure username and password for Traefik"""
        self.password_string = cmd(f'echo $(htpasswd -nb {self.username} {self.password}) | sed -e s/\\$/\\$\\$/g', capture_output=True)

    def configure_docker_compose(self):
        """Configure Docker Compose for Traefik"""
        
        with open(self.directory / 'docker-compose.yml', 'w') as f:
            yaml.dump({
                'version': '3',
                'services': {
                    'traefik': {
                        'image': 'traefik:latest',
                        'container_name': 'traefik',
                        'restart': 'unless-stopped',
                        'security_opt': [
                            'no-new-privileges:true'
                        ],
                        'networks': [
                            'proxy'
                        ],
                        'ports': [
                            '80:80',
                            '443:443'
                        ],
                        'volumes': [
                            '/etc/localtime:/etc/localtime:ro',
                            '/var/run/docker.sock:/var/run/docker.sock:ro',
                            '/opt/containers/traefik/data/traefik.yml:/traefik.yml:ro',
                            '/opt/containers/traefik/data/acme.json:/acme.json',
                            '/opt/containers/traefik/data/dynamic_conf.yml:/dynamic_conf.yml'
                        ],
                        'labels': [
                            f'traefik.enable=true',
                            f'traefik.http.routers.traefik.entrypoints=http',
                            f'traefik.http.routers.traefik.rule=Host(`traefik.{self.domain}`)',
                            f'traefik.http.middlewares.traefik-auth.basicauth.users={self.password_string}',
                            f'traefik.http.middlewares.traefik-https-redirect.redirectscheme.scheme=https',
                            f'traefik.http.routers.traefik.middlewares=traefik-https-redirect',
                            f'traefik.http.routers.traefik-secure.entrypoints=https',
                            f'traefik.http.routers.traefik-secure.rule=Host(`traefik.{self.domain}`)',
                            f'traefik.http.routers.traefik-secure.tls=true',
                            f'traefik.http.routers.traefik-secure.tls.certresolver=http',
                            f'traefik.http.routers.traefik-secure.service=api@internal',
                            f'providers.file.filename=/dynamic_conf.yml',
                            f'traefik.http.routers.traefik-secure.middlewares=secHeaders@file,traefik-auth'
                        ]
                    }
                },
                'networks': {
                    'proxy': {
                        'external': True
                    }
                }
            }, f)

    def build_docker_container(self):
        """Create a Docker network and build the Docker container"""

        cmd('docker network create proxy')
        cmd(f'docker-compose -f {self.directory / "docker-compose.yml"} up -d')

        print('[i]Waiting for container to start[/i] ', end="")
        for _ in range(30):
            print('.', end="")
            time.sleep(1)
        print()

        cmd('docker-compose logs')