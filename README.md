# Homeserver Quick Installation

A Python program to help you get your own home server set up in no time.
It follows to the point a guide written by [Jung Chae-san](https://gitlab.com/jungchaesan), [A Self Hosting Guide for Beginners](https://gitlab.com/jungchaesan/self-hosting-guide-for-beginners/-/blob/main/README.md).