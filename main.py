from rich import print

from docker import install_docker
from docker import install_docker_compose


def main():
    """Guided setup for Docker, Traefik, and Synapse"""

    install_docker()

    print('Which version of Docker Compose to install?')
    print('Preferably, you should find and enter the latest version from:')
    print('https://github.com/docker/compose/releases')
    version = input('Version: ')

    install_docker_compose(version)


if __name__ == '__main__':
    main()